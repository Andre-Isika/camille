// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config : {
    apiKey: "AIzaSyBt2gVSP2glf93puyzVpZyh9QjMcoTAmAQ",
    authDomain: "ljref-5b9af.firebaseapp.com",
    databaseURL: "https://ljref-5b9af.firebaseio.com",
    projectId: "ljref-5b9af",
    storageBucket: "ljref-5b9af.appspot.com",
    messagingSenderId: "90205190015",
    appId: "1:90205190015:web:fde2b555b9d0b7dbbc4c4a",
    measurementId: "G-FECRXC7NQK"
  }
  // msLogin: 'http://localhost:9999', 
  // msStats: 'http://localhost:9996',
  // msSauvegarde: 'http://localhost:9995',
  // urlEtablissement: 'https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/?statut_diffusion=O&etat_administratif=A',
  // urlDepartement: 'https://geo.api.gouv.fr/departements/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
